import org.junit.Before;
import org.junit.Test;

import junit.framework.*;

import javax.xml.soap.Node;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class TestGraph {

    Graph graph;
    Fragment[] frags;

    boolean wihtoutFactor;

    @Before
    public void setUp(){

        wihtoutFactor = true;
        frags = new Fragment[3];
        //AACCCAA
        frags[0] = new Fragment("ATGC");
        frags[1] = new Fragment("TGCTA");
        frags[2] = new Fragment("GGG");
    }
    
    @Test
    public void sortAlignment(){
        LinkedList<char[]> res = new LinkedList<>();
        res.add(new char[] {'a', 'b'});
        res.add(new char[] {'c', 'd'});
        res.add(new char[] {'e', 'c'});
        res.add(new char[] {'b', 'e'});

        System.out.print("res = ");
        for (char[] c : res){
            System.out.print("(" + c[0] + " , " + c[1] + ") - ");
        }
        System.out.println("\n");

        LinkedList<char[]> res2 = new LinkedList<>();
        LinkedList<char[]> temp = new LinkedList<>();
        temp.addLast(res.pop());
        while(res.size() > 0){
            boolean found = false;
            // System.out.print("Compare (" + temp.getFirst()[0] + ", " + temp.getFirst()[1] + ")");
            for (int i = 0; i<res.size(); i++) {
                char[] a2 = res.get(i);
                // System.out.println(" with (" + a2[0] + ", " + a2[1] + ")");
                if (temp.getLast()[1] == a2[0]) {
                    // System.out.println("Found that second was equal to first");
                    temp.addLast(a2);
                    res.remove(i);
                    found = true;
                    break;
                } else if (temp.getFirst()[0] == a2[1]) {
                    // System.out.println("Found that first was equal to second");
                    temp.addFirst(a2);
                    res.remove(i);
                    found = true;
                    break;
                }
            }if (!found){
                // System.out.print("temp = ");
                for (char[] c : temp){
                    // System.out.print("(" + c[0] + " , " + c[1] + ") - ");
                }
                //System.out.println("\n");
                res2.addAll(temp);
                temp.clear();
                temp.addLast(res.pop());
            }else if (res.size() == 0)
                res2.addAll(temp);
        }

        System.out.print("res2 = ");
        for (char[] c : res2){
            System.out.print("(" + c[0] + " , " + c[1] + ") - ");
        }
        System.out.println("\n");
    }

    @Test
    public void greedySortedArc(){
        FragParser fp = new FragParser("/home/isma/git-repositories/bioinfo/ressources/10000/collection2.fasta");
        frags = fp.getFrags();
        graph = new Graph(frags);
        System.out.println(graph.getArc());
        LinkedList<Alignment> res = graph.runGreedy(wihtoutFactor);
        System.out.println("res.size() = " + res.size());
        System.out.println(res);
    }

    @Test
    public void testFindSet(){
        Graph.Node f = new Graph.Node(frags[0], frags[0].getInvComp());
        Graph.Node g = new Graph.Node(frags[1], frags[1].getInvComp());
        HashSet<Fragment> x = new HashSet<>();
        HashSet<Fragment> y = new HashSet<>();
        HashSet<Fragment> z = new HashSet<>();
        HashSet<Fragment> xx = new HashSet<>();
        x.add(f.getFragment()); y.add(g.getFragment());
        xx.add(f.getFragmentComp()); z.add(f.getFragmentComp());
        LinkedList<HashSet<Fragment>> w = new LinkedList<>();
        w.addLast(x); w.addLast(y); w.add(z);
        w.add(xx);
        int[] res = Graph.findSet(w, f.getFragment(), g.getFragment());
        Assert.assertEquals(0, res[0]);
        Assert.assertEquals(1, res[1]);
    }

    @Test
    public void testConstruct(){
        graph = new Graph(frags);
        System.out.println("Taille des arcs réels: " + graph.arcSize());
        double x = 8*(graph.size()*(graph.size()-1));
        System.out.println("Taille des arcs théoriques: " + x/2);
        Assert.assertTrue(graph.arcSize() == x/2);
        System.out.println(graph.getArc());
        System.out.println(graph.runGreedy(!wihtoutFactor));
    }

    @Test
    public void smallTest(){
        FragParser fp = new FragParser("/home/isma/git-repositories/bioinfo/ressources/10000/collection2.fasta");
        frags = fp.getFrags();
        graph = new Graph(frags);
        System.out.println(graph.runGreedy(wihtoutFactor));
    }

    @Test
    public void realTest(){
        FragParser fp = new FragParser("/home/isma/git-repositories/bioinfo/ressources/10000/collection1.fasta");

        frags = fp.getFrags();
        long t1 = System.currentTimeMillis();
        graph = new Graph(frags);
        long t2 = System.currentTimeMillis();
        System.out.println("Construction du graphe: " + ((t2-t1)/1000) + "s");
        LinkedList<Alignment> alignments = graph.runGreedy(!wihtoutFactor);
        t1 = System.currentTimeMillis();
        System.out.println("Algorithme greedy du graphe: " + ((t1-t2)/1000) + "s");
        //System.out.println(alignments);
        Consensus c=new Consensus(alignments);
        String k=c.computeSupersequence(alignments);
        try{
            File f=new File("output/resultats/result1.fasta");
            FileWriter fw=new FileWriter(f);
            fw.write(k);
            fw.close();
            System.out.println("The sequence has been saved in "+f.getAbsolutePath());
        }catch(Exception e){}
    }

    @Test
    public void realTest4(){
        FragParser fp = new FragParser("/home/isma/git-repositories/bioinfo/ressources/11200/collection4.fasta");

        frags = fp.getFrags();
        long t1 = System.currentTimeMillis();
        graph = new Graph(frags);
        long t2 = System.currentTimeMillis();
        System.out.println("Construction du graphe: " + ((t2-t1)/1000) + "s");
        LinkedList<Alignment> alignments = graph.runGreedy(wihtoutFactor);
        t1 = System.currentTimeMillis();
        System.out.println("Algorithme greedy du graphe: " + ((t1-t2)/1000) + "s");
        //System.out.println(alignments);
        Consensus c=new Consensus(alignments);
        String k=c.computeSupersequence(alignments);
        try{
            File f=new File("output/resultats/result4_sf.fasta");
            FileWriter fw=new FileWriter(f);
            fw.write(k);
            fw.close();
            System.out.println("The sequence has been saved in "+f.getAbsolutePath());
        }catch(Exception e){}
    }

    @Test
    public void bigTest(){
        FragParser fp = new FragParser("/home/isma/git-repositories/bioinfo/ressources/100000/collection2.fasta");

        frags = fp.getFrags();
        long t1 = System.currentTimeMillis();
        graph = new Graph(frags);
        long t2 = System.currentTimeMillis();
        System.out.println("Construction du graphe: " + ((t2-t1)/1000) + "s");
        LinkedList<Alignment> alignments = graph.runGreedy(wihtoutFactor);
        t1 = System.currentTimeMillis();
        System.out.println("Algorithme greedy du graphe: " + ((t1-t2)/1000) + "s");
        //System.out.println(alignments);
        Consensus c=new Consensus(alignments);
        String k=c.computeSupersequence(alignments);
        try{
            File f=new File("output/resultats/result2_sf.fasta");
            FileWriter fw=new FileWriter(f);
            fw.write(k);
            fw.close();
            System.out.println("The sequence has been saved in "+f.getAbsolutePath());
        }catch(Exception e){}
    }

    @Test
    public void dotMatcher(){
        FragParser fp = new FragParser("/home/isma/git-repositories/bioinfo/ressources/10000/collection1.fasta");

        frags = fp.getFrags();
        long t1 = System.currentTimeMillis();
        graph = new Graph(frags);
        long t2 = System.currentTimeMillis();
        System.out.println("secondes: " + ((t2-t1)/1000));
        LinkedList<Alignment> alignments = graph.runGreedy(wihtoutFactor);
        //System.out.println(alignments);

        for (int i = 0; i < alignments.size(); i++){
            FileWriter toWrite;
            try {
                Alignment n = alignments.get(i);
                toWrite = new FileWriter("input/alignements/fandg" + i);
                toWrite.write(new Fragment(n.fragment1.toString().replace("-", "")).toString() + "-------" + new Fragment(n.fragment2.toString().replace("-", "")).toString());
                toWrite.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < alignments.size(); i++){
            FileWriter toWrite;
            try {
                Alignment n1 = alignments.get(i);
                toWrite = new FileWriter("input/alignements/preview" + i);
                toWrite.write(new Fragment(n1.fragment1.toString().replace("-", "")).toString());
                toWrite.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        /*
        for (int i = alignments.size()-1; i > 0; i--){
            FileWriter toWrite;
            try {
                Alignment n = alignments.get(i);
                toWrite = new FileWriter("input/test1_" + number);
                toWrite.write(new Fragment(n.fragment1.toString()).getInvComp().toString());
                toWrite.close();
                number++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        */
    }
}
