import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.BitSet;

public class Fragment {

    public static final int FRAG_LEN_BY_DEFAULT = 500;

    //private BitSet[] fragment;
    private Proteine[] fragment;
    private Fragment fragmentComp;

    @Deprecated
    public Fragment(Fragment fragment) {
        this(fragment.getFragment());
    }

    /**
     * Construit un fragment ainsi qu'une référence vers son complémentaire
     * @param fragment
     */
    /*
    public Fragment(BitSet[] fragment){
        this(fragment, true);
    }
     */

    /**
     * Construit un fragment ainsi qu'une référence vers son complémentaire
     * @param fragment
     */
    public Fragment(Proteine[] fragment){
        this(fragment, true);
    }

    /**
     * Construis un fragment avec ou sans sa référence vers son complémentaire
     * @param fragment le bitset à construire
     * @param withComp true si on doit construire le complémentaire, false sinon
     */
    public Fragment(Proteine[] fragment, boolean withComp){
        this.fragment = fragment;
        if (withComp) {
            int N = fragment.length;
            Proteine[] newFrag = new Proteine[N];
            for (int i = 0; i < fragment.length/2.; i++) {
                Proteine first = fragment[N - i - 1].clone();
                Proteine last = fragment[i].clone();
                first.flip();
                last.flip();
                newFrag[i] = first;
                newFrag[N - i - 1] = last;
            }
            fragmentComp = new Fragment(newFrag, false);
        }
    }

    /*
    public Fragment(BitSet[] fragment, boolean withComp){
        this.fragment = fragment;
        if (withComp) {
            int N = fragment.length;
            BitSet[] newFrag = new BitSet[N];
            for (int i = 0; i < fragment.length/2; i++) {
                BitSet first = (BitSet) fragment[N - i - 1].clone();
                BitSet last = (BitSet) fragment[i].clone();
                first.flip(0, 3);
                last.flip(0, 3);
                newFrag[i] = first;
                newFrag[N - i - 1] = last;
            }
            fragmentComp = new Fragment(newFrag, false);
        }
    }
     */

    public Fragment(String frag){
        this(FragParser.toProteineMatrix(frag));
    }

    public Proteine[] getFragment() {
        return fragment;
    }

    public int getLength(){
        return fragment.length;
    }

    /**
     * Calcule le complémentaire inversé d'un fragment
     * @return le fragment complémentaire inversé en binaire
     */
    public Fragment getInvComp(){
        return this.fragmentComp;
    }

    public Proteine getChar(int index){
        return this.fragment[index];
    }

    public boolean equalStrip(Fragment f){
        int j = 0;
        for (int i = 0; i<this.fragment.length; i++){
            //System.out.println("Testing: " + this.getChar(i));
            if (!this.getChar(i).equals(getSpace())) {
                if (j >= f.getLength())
                    return false;
                boolean found = false;
                do{
                    //System.out.println("WITH: " + f.getChar(j));
                    if (!f.getChar(j).equals(getSpace())){
                        if (this.equals(i, f, j, 1) == 1) {
                            //System.out.println("found");
                            found = true;
                        }else
                            return false;
                    }j++;
                }while(j<f.getLength() && !found);
            }
        }
        while (j < f.getLength()){
            if (!f.getChar(j).equals(getSpace())){
                return false;
            }
            j++;
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        Fragment f = (Fragment)o;
        if (fragment.length != f.fragment.length)
            return false;
        for (int i = 0; i<fragment.length; i++){
            if (this.equals(i, f, i, 1) != 1)
                return false;
        }
        return true;
    }

    /**
     * Compare deux caractère en binaire
     * @param index1 l'index dans le premier fragment à comparer
     * @param frag2 le deuxième fragment où le caractère se trouve
     * @param index2 l'index dans le deuxième fragment à comparer
     * @param coeff le coefficient de score à retourner en cas d'égalite
     * @return coeff si égal, -coeff sinon
     */
    public int equals(int index1, Fragment frag2, int index2, int coeff){

        if (frag2.getChar(index2).getByte() == this.fragment[index1].getByte())
            return coeff;
        else
            return -coeff;

        /*
        //77 secondes sur le fichier 10000
        if (this.fragment[index1].equals(frag2.getChar(index2)))
            return coeff;
        else
            return -coeff;
        */

        /*
        //84 secondes pour le fichier 10000
        if (this.fragment[index1].get(0) == frag2.getChar(index2).get(0)
            && this.fragment[index1].get(1) == frag2.getChar(index2).get(1)
            && this.fragment[index1].get(2) == frag2.getChar(index2).get(2))
            return coeff;
        else
            return -coeff;
        */

        /*
        //194 secondes pou le fichier 10000
        BitSet compare = (BitSet) this.fragment[index1].clone();
        compare.xor(frag2.getChar(index2));
        BitSet zero = new BitSet(3);
        if (compare.equals(zero)){
            return coeff;
        }else{
            return -coeff;
        }*/
    }

    public static Proteine getSpace(){
        return new Proteine(0);
    }


    /**
     * Affiche un tableau de BitSet en fragment AGGCT...
     * @return le fragment en string
     */
    @Override
    public String toString(){
        return String.valueOf(this.convertBin2String(this.fragment));
    }

    /**
     * Récupère la valeur en String d'un caractère codé en binaire
     * @param index l'index du caractère dans le fragment en binaire
     * @return le caractère en String
     */
    public StringBuilder getSt0(int index){
        return this.convertProt2St0(this.fragment[index]);
    }

    /**
     * Retourne la lettre associé à un codage en binaire pour un caractère seulement
     * @param binChar le caractère en binaire à convertir
     * @return A,G,T,C
     */
    public StringBuilder binChar2String(String binChar){
        StringBuilder res = new StringBuilder();
        switch (binChar) {
            case "1":
                res.append("A");
                break;
            case "2":
                res.append("T");
                break;
            case "3":
                res.append("G");
                break;
            case "4":
                res.append("C");
                break;
            default:
                res.append("-");
                break;
        }
        return res;
    }
    /*
    public StringBuilder binChar2String(String binChar){
        StringBuilder res = new StringBuilder();
        switch (binChar) {
            case "001":
                res.append("A");
                break;
            case "110":
                res.append("C");
                break;
            case "010":
                res.append("G");
                break;
            case "101":
                res.append("T");
                break;
            default:
                res.append("-");
                break;
        }
        return res;
    }
     */

    public StringBuilder convertProt2St0(Proteine prot){
        return new StringBuilder(binChar2String(prot.toString()));
    }

    /*
    public StringBuilder convertBin2St0(BitSet b){
        StringBuilder res = new StringBuilder();
        String s = "";
        for(int i = 0; i < 3; i++){
            //gérer le cas 0/"
            if ((i==0 && b.get(0)) || b.get(i))
                s += "1";
            else
                s += "0";
            if (i == 2){

                res.append(this.binChar2String(s));
                s = "";
            }
        }
        return res;
    }*/

    private StringBuilder convertBin2String(Proteine[] fragment){
        StringBuilder res = new StringBuilder();
        String s = "";
        for(int i = 0; i < fragment.length; i++){
            //gérer le cas 0/"
            res.append(convertProt2St0(fragment[i]));
        }
        return res;
    }
    /*
    private StringBuilder convertBin2String(BitSet[] fragment){
        StringBuilder res = new StringBuilder();
        String s = "";
        for(int i = 0; i < fragment.length*3; i++){
            //gérer le cas 0/"
            if ((i==0 && fragment[0].get(0)) || fragment[i/3].get(i%3))
                s += "1";
            else
                s += "0";
            if (i%3 == 2){
                res.append(this.binChar2String(s));
                s = "";
            }
        }
        return res;
    }
     */
}
