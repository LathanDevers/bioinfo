import java.util.BitSet;

/**
 * ComputeAlignmentThread
 * @version 1
 * @author Daniels M.
 */

public class ComputeAlignmentThread{


    public final int GAPSCORE=-2;
    public final int FACTORSCORE=Integer.MAX_VALUE;
    private Fragment fragment1,fragment2;
    int weight;
    Adj adjacenceMatrix;
    Alignment alignment;
    int x,y;


    public ComputeAlignmentThread(int weight,Fragment fragment1,Fragment fragment2,Adj adjacenceMatrix,int[] position){
        this.fragment1=fragment1;
        this.fragment2=fragment2;
        this.weight=weight;
        this.adjacenceMatrix=adjacenceMatrix;
        this.x=position[0];
        this.y=position[1];
        run();
    }


    public void run(){
        Fragment fragmentA1;
        Fragment fragmentA2;

        String s=this.fragment1.toString();
        String t=this.fragment2.toString();

        String s1="";
        String t1="";
        
        //System.out.println("compute_alignment "+this.x+" "+this.y);

        int kx=0;
        int ky=0;

        if (x<s.length()) {
            s1+=s.substring(x);
            t1+=addSpace(s.length()-x);
            kx++;
        }

        if(y<t.length()){
            s1+=addSpace(t.length()-y);
            t1+=t.substring(y);
            ky++;
        }
        
        while (x>0 && y>0) {
            if (this.adjacenceMatrix.get(x-1,y)==this.adjacenceMatrix.get(x,y)-GAPSCORE) {
                s1=s.charAt(x-1)+s1;
                t1="-"+t1;
                x--;
            }else if (this.adjacenceMatrix.get(x,y-1)==this.adjacenceMatrix.get(x,y)-GAPSCORE) {
                s1="-"+s1;
                t1=t.charAt(y-1)+t1;
                y--;
            }else if (this.adjacenceMatrix.get(x-1,y-1)==this.adjacenceMatrix.get(x,y)-SemiV2.pScore(fragment1, fragment2, x-1, y-1)) {
                s1=s.charAt(x-1)+s1;
                t1=t.charAt(y-1)+t1;
                x--;
                y--;
            }
        }

        if (x!=0) {
            s1=s.substring(0,x)+s1;
            t1=addSpace(x)+t1;
            kx--;
        }

        if (y!=0) {
            s1=addSpace(y)+s1;
            t1=t.substring(0, y)+t1;
            ky--;
        }

        /*if (kx==0 || ky==0) {
            this.weight=-FACTORSCORE;
        }*/

        fragmentA1=FragParser.toProteineMatrix(s1);
        fragmentA2=FragParser.toProteineMatrix(t1);
        this.alignment=new Alignment(this.weight,fragmentA1,fragmentA2);
    }

    private String addSpace(int n){
        String res="";
        for (int i = 0; i < n; i++) {
            res="-"+res;
        }
        return res;
    }

    public Alignment getAlignment(){
        return this.alignment;

    }
}