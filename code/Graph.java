import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

class Graph{


    //Construction du graphe: si n fragments, O(2n) = O(n)
    //Potentiellement construire ça en arbre binaire de recherche
    //Tri des poids pour l'algo greedy: O(200)=O(1)
    //  ->si arcs stocké dans une table de hashage en fonction de leur poids
    //  -> HashTable<K, V> où K=poids et V=arc
    //Suppression des noeuds déjà utilisé (ainsi que leur complémentaire)
    //  ->1° Si ABR alors "facile" de retrouver en temps raisonnable un fragments ainsi que son complémentaire
    //  ->2° On peut aussi stocker les fragments par paire (fragment-complémentiare) ainsi accès direct au noeud
    //List d'adjacence(sous forme de table de hashage):
    //  ->AAGTGCTGA -- [GTAGCGAT, AAGCTGAGTCGA, AAGCGA, GGTCGCA]
    //  ->(1,1') - [2, 3, 4, 5]
    //  ->2 - [1, 3, 4, 5, 1', 5]
    //  ->3 - [1, 4]
    //  ->5- []
    //  ->1 - [(3, 0)]

    //Suppresion d'un noeud:
    //  ->tous les arcs sortants (facile)
    //  ->tous les arcs entrants (chiant car doit parcourir toutes les liste)
    //      ->essayer de stocker dans une autre table les arcs sortants

    //Arbre binaire par noeud: à droite les arcs sortants
    //à gauche les noeuds entrants


/*
    private Fragment[] frags;
    private Fragment[] fragsC;
    */

    private final Hashtable<Integer, LinkedList<Arc>> weigth;
    private ArrayList<Node> nodes;
    private int maxweight;

    private double size, arcSize;


    public Graph(Fragment[] fragments){
        nodes = new ArrayList<>(fragments.length);
        size = fragments.length;
        arcSize = 0;
        weigth = new Hashtable<>();
        maxweight = 0;
        int val = -1;
        int precVal;
        for (int i = 0; i<fragments.length; i++){
            Node n = new Node(fragments[i],fragments[i].getInvComp());
            nodes.add(n);
        }
        for(int i = 0; i < fragments.length; i++){
            precVal = val;
            val = (int)Math.floor((i/(float)fragments.length)*100);
            this.loadingBar(fragments, i, precVal, val);
            Node n = nodes.get(i);
            for (int j = i+1; j < fragments.length; j+=3) {
                arcSize+=8;
                Fragment fi = fragments[i];
                //38 secondes avec 2 threads secondaires
                int x = i;
                int y = j;
                Thread t1 = null;
                Thread t2 = null;
                if (j < fragments.length-1) {
                    arcSize+=8;
                    t1 = new Thread(() -> computeArc(n, fragments, x, y+1, "Thread 1"));
                    t1.start();
                }

                if (j < fragments.length-2) {
                    arcSize+=8;
                    t2 = new Thread(() -> computeArc(n, fragments, x, y+2, "Thread 2"));
                    t2.start();
                }
                this.computeArc(n, fragments, i, j, "Thread principal");
                try {
                    t1.join();
                    t2.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (NullPointerException e){

                }
            }

        }
        //System.out.println("\nmaxweight = " + maxweight);
    }

    /**
     * Retourne les sets dans lesquels se trouve f et g
     * @param sets la liste des ensembles
     * @param f l'ensemble à trouver
     * @param g l'ensemble à trovuer
     * @return un tableau contenant les index des set cherché dans la liste
     */
    public static int[] findSet(LinkedList<HashSet<Fragment>> sets, Fragment f, Fragment g){
        int[] res = {-1,-1};
        int i = 0;
        for (Set<Fragment> s : sets){
            //System.out.println("On cherche dans s = " + s);
            if (s.contains(f)) {
                //System.out.println("on a trouvé f: " + f);
                res[0] = i;
            }if (s.contains(g)) {
                //System.out.println("on a trouvé g: " + g);
                res[1] = i;
            }
            i++;
            /*
            for (Node n : s){
                if (f.equals(n)) {
                    System.out.println("On a trouvé " + f + " dans " + s);
                    res[0] = sets.indexOf(s);
                }if (g.equals(n)) {
                    System.out.println("On a trouvé " + g + " dans " + s);
                    res[1] = sets.indexOf(s);
                }
            }
             */
        }
        return res;
    }

    private void computeArc(Node n, Fragment[] fragments, int i, int j, String toprint) {
        Fragment fi = fragments[i];
        Fragment fj = fragments[j];
        Node _n = nodes.get(j);
        SemiV2 s = new SemiV2(fi, fj);
        /*
        if (n.usable){
            if (_n.usableC)
                s = new SemiV2(fi, fj, true, true);
            else
                s = new SemiV2(fi, fj, true, false);
        }else{
            if (_n.usableC)
                s = new SemiV2(fi, fj, false, true);
            else
                s = new SemiV2(fi, fj, false, false);
        }
        */
        int[] poids = s.getWeights();
        //Alignment[] a = s.getAlignments();
        //todo éviter de recréer un noeud j à chaque fois avec frag et fragC ptet ou nodes
        for (int k = 0; k < poids.length; k++) {
            Arc arcToAdd;
            //boolean factor = s.factor(k);
            //System.out.println("n = " + n + " - " + _n + " factor: " + factor);
            if (poids[k] > maxweight)
                maxweight = poids[k];
            switch (k) {
                case 0://f->g
                    arcToAdd = new Arc(n, _n, false, false);
                    this.addPoids(poids[k], arcToAdd);
                    break;
                case 1://f->g'
                    arcToAdd = new Arc(n, _n, false, true);
                    this.addPoids(poids[k], arcToAdd);
                    break;
                case 2://f'->g
                    arcToAdd = new Arc(n, _n, true, false);
                    this.addPoids(poids[k], arcToAdd);
                    break;
                case 3://f'->g'
                    arcToAdd = new Arc(n, _n, true, true);
                    this.addPoids(poids[k], arcToAdd);
                    break;
                case 4://g->f
                    arcToAdd = new Arc(_n, n, false, false);
                    this.addPoids(poids[k], arcToAdd);
                    break;
                case 5://g->f'
                    arcToAdd = new Arc(_n, n, false, true);
                    this.addPoids(poids[k], arcToAdd);
                    break;
                case 6://g'->f
                    arcToAdd = new Arc(_n, n, true, false);
                    this.addPoids(poids[k], arcToAdd);
                    break;
                case 7://g'->f'
                    arcToAdd = new Arc(_n, n, true, true);
                    this.addPoids(poids[k], arcToAdd);
                    break;
                default:
                    System.err.println("Erreur:  La liste des poids est trop grande");
                    break;
            }
        }
    }

    /**
     * Ajoute l'arc de poids i dans la table de hashage des arcs
     * @param i le poids i
     * @param arc l'arc à ajouter
     */
    /*
    private void addPoids(Integer i, Arc arc){
        this.weigth.put(i, arc);
    }*/

    private void loadingBar(Fragment[] fragments, int i, int precVal, int val) {
        if (precVal < 0 || precVal != val) {
            System.out.print("Loading " + val + "% - " + i + "/" + fragments.length + "\r");
        }
    }

    /**
     * Ajoute un arc dans la table des arcs
     * @param i le poids de l'arc à ajouter
     * @param arc l'arc à ajouter
     */
    private void addPoids(Integer i, Arc arc){
        if (this.weigth.get(i) == null) {
            LinkedList<Arc> x = new LinkedList<>();
            x.add(arc);
            this.weigth.put(i, x);
        }else {
            synchronized (this.weigth.get(i)) {
                this.weigth.get(i).addLast(arc);
            }
        }

    }

    public Hashtable<Integer, LinkedList<Arc>> getArc(){
        return weigth;
    }

    /**
     * Retourne le nombre d'arc dans le graphe.
     * @return
     */
    public double size(){
        return this.size;
    }

    public double arcSize(){
        return this.arcSize;
    }

    /**
     * Lance l'algorithme greedy sur le graphe
     * @return
     */
    public LinkedList<Alignment> runGreedy(boolean withoutFactor){
        LinkedList<Alignment> res = new LinkedList<>();
        ArrayList<Fragment> write = new ArrayList<>();
        ArrayList<Fragment> write2 = new ArrayList<>();
        LinkedList<HashSet<Fragment>> sets = new LinkedList<>();
        for (Node node : nodes) {
            HashSet<Fragment> mySet1 = new HashSet<>();
            HashSet<Fragment> mySet2 = new HashSet<>();
            mySet1.add(node.getFragment()); mySet2.add(node.getFragmentComp());
            sets.add(mySet1); sets.add(mySet2);
        }

        int i = maxweight;
        // int i = 700;
        boolean first = true;
        boolean type = false;
        while (i >= 0 && sets.size() > 1 && res.size() < nodes.size()){
            //System.out.println("-------------" + i +"-------------");
            if (weigth.get(i) == null || weigth.get(i).size() == 0){
                weigth.remove(i);
                i--;
            }else{
                Arc arc = weigth.get(i).getFirst();
                /*
                System.out.println("-------------" + i +"-------------");
                System.out.println(arc);
                System.out.println("arc.usable() = " + arc.usable());
                System.out.println("arc.notInAndOut() = " + arc.notInAndOut());
                */
                if (!arc.usable() || arc.notInAndOut()) {
                    weigth.get(i).removeFirst();
                }else{
                    int[] sol = findSet(sets, arc.getF(), arc.getG());
                    if (sol[0] != sol[1]) {
                        //si oui il faut savoir de quel type est l'arc pour correctement set nos
                        //variable usable, in et out
                        Alignment toAdd = SemiV2.computAlignment(arc.getF(), arc.getG());
                        if (withoutFactor && toAdd.poids >= 0) {
                            //System.out.println("ADDED");
                            //write.add(arc.getF());
                            //write2.add(arc.getG());
                            res.addLast(toAdd);
                            arc.setInOut();
                            arc.setNodeFalse();
                            sets.get(sol[0]).addAll(sets.get(sol[1]));
                            sets.remove(sol[1]);
                        }else if (!withoutFactor){
                            //write.add(arc.getF());
                            //write2.add(arc.getG());
                            res.addLast(toAdd);
                            arc.setInOut();
                            arc.setNodeFalse();
                            sets.get(sol[0]).addAll(sets.get(sol[1]));
                            sets.remove(sol[1]);
                        }
                    }weigth.get(i).removeFirst();
                }
            }
        }
        /*
        int number = 0;
        for (Fragment n : write){
            FileWriter toWrite;
            try {
                toWrite = new FileWriter("input/arcs/arcf" + number);
                toWrite.write(n.toString());
                toWrite.close();
                number++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        number = 0;
        for (Fragment n : write2){
            FileWriter toWrite;
            try {
                toWrite = new FileWriter("input/arcs/arcg" + number);
                toWrite.write(n.toString());
                toWrite.close();
                number++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
         */
        return this.sortAlignment(res);
    }

    private LinkedList<Alignment> sortAlignment(LinkedList<Alignment> res) {
        LinkedList<Alignment> res2 = new LinkedList<>();
        LinkedList<Alignment> temp = new LinkedList<>();
        temp.addLast(res.pop());
        while(res.size() > 0){
            boolean found = false;
            for (int i = 0; i<res.size(); i++) {
                Alignment a2 = res.get(i);
                if (temp.getLast().fragment2.equalStrip(a2.fragment1)) {
                    temp.addLast(a2);
                    res.remove(i);
                    found = true;
                    break;
                }else if (temp.getFirst().fragment1.equalStrip(a2.fragment2)) {
                    temp.addFirst(a2);
                    res.remove(i);
                    found = true;
                    break;
                }
            }if (!found){
                System.err.println("----- NOUVEAU ----" );
                res2.addAll(temp);
                temp.clear();
                temp.addLast(res.pop());
            }else if (res.size() == 0)
                res2.addAll(temp);
        }
        for (int i = 0; i < res2.size()-1; i++){
            if (!res2.get(i).fragment2.toString().replace ("-", "").equals(res2.get(i+1).fragment1.toString().replace("-", ""))){
                System.err.println("PAS EGAL OSCOUR");
            }
        }
        return res2;
    }

    static class Node{
        Fragment fragment, fragmentComp;
        boolean usable, usableC;

        boolean in, out, inC, outC;

        /**
         * Construit un Noeud sur base de son fragment ainsi que de son complémentaire
         * @param fragment le fragment
         * @param fragmentComp son complémentaire
         */
        Node(Fragment fragment, Fragment fragmentComp){
            this.fragment = fragment;this.fragmentComp=fragmentComp;
            usable = true;
            usableC = true;
            in = false; inC = false;
            out = false; outC = false;
        }

        /**
         * Retourne le fragment
         * @return un fragment de type Fragment
         */
        public Fragment getFragment() {
            return fragment;
        }

        /**
         * Retourne le complémentaire de son fragment
         * @return un fragment de type Fragment
         */
        public Fragment getFragmentComp() {
            return fragmentComp;
        }

        /**
         * Etabli le noeud comme non éligible au passage d'un chemin hamiltonien:
         *  soit parce qu'il a déjà été pris
         *  soit parce que son complémentaire à été pris
         * @param isComp vrai si on set a non usable le fragment complementaire, faux sinon
         */
        public void setFalse(boolean isComp){
            if (isComp)
                this.usableC = false;
            else
                this.usable = false;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Node)) {
                return false;
            }
            Node n = (Node)o;
            return this.fragment.equals(n.fragment) && this.fragmentComp.equals(n.fragmentComp);
        }

        @Override
        public int hashCode() {
            return Objects.hash(fragment, fragmentComp);
        }

        @Override
        public String toString() {
            return fragment.toString();
        }

    }

    class Arc{

        boolean isComp1, isComp2;
        private Node f, g;

        /**
         *
         * @param n1 Noeud f
         * @param n2 vers Noeud g
         * @param isComp1 s'il faut prendre le le complémentaire du fragment dans le noeud n1
         * @param isComp2 s'il faut prendre le complémentaire du fragment dans le noeud n2
         */
        Arc(Node n1, Node n2, boolean isComp1, boolean isComp2){
            this.f = n1;this.g=n2;
            this.isComp1 = isComp1;
            this.isComp2 = isComp2;

        }

        public boolean isFComp() {
            return isComp1;
        }

        public boolean isGComp() {
            return isComp2;
        }


        /**
         * Retourne le type d'un arc càd la direction de son arc
         * @return
         */
        ArcType getType(){
            if (!isComp1) {
                if (!isComp2)
                    return ArcType.F2G;
                else
                    return ArcType.F2GP;
            }else{
                if (!isComp2)
                    return ArcType.FP2G;
                else
                    return ArcType.FP2GP;
            }
        }

        Fragment getF(){
            if (!isComp1)
                return f.getFragment();
            else
                return f.getFragmentComp();
        }

        public Fragment getG(){
            if (!isComp2)
                return g.getFragment();
            else
                return g.getFragmentComp();
        }

        public Node getFNode(){
            return this.f;
        }

        public Node getGNode(){
            return this.g;
        }

        /**
         * Détermine si un arc peut-être utilisé ou non lors de greedy
         * Si un des deux noeuds n'est pas utilisable car un de ses complémentaire a été utilisé alors Arc.usable() retourne faux
         * @return vrai si l'arc est utilisable, faux sinon
         */
        boolean usable(){
            if (!isComp1) {
                if (!isComp2)
                    return f.usableC && g.usableC;
                else
                    return f.usableC && g.usable;
            }else
                if (!isComp2)
                    return f.usable && g.usableC;
                else
                    return f.usable && g.usable;
        }

        /**
         * Spécifie si on est déjà sorti ou entré d'un des noeuds (def from greedy)
         * @return vrai si g.in et f.out est vrai, faux sinon
         */
        boolean notInAndOut(){
            if (isComp1){
                if (isComp2)
                    return g.inC || f.outC;
                else
                    return g.in || f.outC;
            }else{
                if (isComp2)
                    return g.inC || f.out;
                else
                    return g.in || f.out;
            }
        }

        /**
         * Etabli qu'on est sorti de du noeud f pour entrer dans le noeud g via les variable f et g.
         */
        public void setInOut(){
            if (!isComp1)
                this.f.out = true;
            else
                this.f.outC = true;
            if (!isComp2)
                this.g.in = true;
            else
                this.g.inC = true;
        }

        @Override
        public String toString(){
            switch (this.getType()){
                case F2G:
                    return f.getFragment().toString() + " -> " + g.getFragment().toString();
                case F2GP:
                    return f.getFragment().toString() + " -> " + g.getFragment().getInvComp().toString() + "*";
                case FP2G:
                    return f.getFragment().getInvComp().toString() + "*" + " -> " + g.getFragment().toString();
                case FP2GP:
                    return f.getFragment().getInvComp().toString() + "*" + " -> " + g.getFragment().getInvComp().toString() + "*";
                default:
                    return "WTF";
            }
        }

        void setNodeFalse() {
            if (!isComp1){
                if (!isComp2) {
                    f.usable = false;
                    g.usable = false;
                }else{
                    f.usable = false;
                    g.usableC = false;
                }
            }else{
                if (!isComp2) {
                    f.usableC = false;
                    g.usable = false;
                }else{
                    f.usableC = false;
                    g.usableC = false;
                }
            }
        }
    }
}