import org.apache.commons.io.input.ReversedLinesFileReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Scanner;

class FragParser{

    //private BitSet[][] fragsMatrix; //matrice des fragments une ligne est un fragment chaque colonne est un bit
    private Fragment[] fragsMatrix;
    @Deprecated
    private BitSet[] frags;

    File file;
    private int collection;


    public FragParser(String filename){
        collection = -1;
        System.out.println("Parsing du fichier " + filename);
        try{
            //Ouverture du fichier
            file = new File(filename);
            //Récupérer le nombre de fragment pour initialiser le tableau
            //Potentiellement milliard de fragment donc éviter arraylist
            int len = this.getFragLen();
            if (len != 0) {
                //frags = new BitSet[len];
                //fragsMatrix = new BitSet[len][Utils.FRAG_LEN_BY_DEFAULT];
                fragsMatrix = new Fragment[len];
                //on suppose que la taille des fragments fait approximativement 500 pour éviter des extensions
                //de tableau constamment puisqu'en pratique ça semble être toujours un peu plus grand que 500
                Scanner scan = new Scanner(file);
                scan.useDelimiter(">");
                while (scan.hasNext()) {
                    String token = scan.next();
                    Tuple posFrag = this.extractToken(token); //get the fragment and the pos from the token
                    //BitSet[] fragment = posFrag.getFrag();
                    Fragment fragment = posFrag.getFrag();
                    //System.out.println("Fragment" + fragment);
                    //System.out.println("Fragment Complémentaire"+fragment.getInvComp());
                    //System.out.println("Fragment Complémentaire = " + fragment.getCompl());
                    fragsMatrix[posFrag.getPos()-1] = posFrag.getFrag();
                }
                scan.close();
                System.out.println("Fin du parsing sans erreur");
            }else
                System.err.println("Aucun fragment chargé");
        }catch(IOException e){
            System.err.println("Impossible d'ouvrir le fichier");
        }
    }
    /**
     * Récupère le nombre de fragments chargés
     * @return le nombre de fragment
     */
    public Fragment[] getFrags(){
        return fragsMatrix;
    }

    /**
     * Récupère le nombre de fragments chargés
     * @return le nombre de fragment
     */
    private int getFragLen(){
        try{
            //todo utilsier la version non déprécié
            ReversedLinesFileReader in = new ReversedLinesFileReader(file);
            String line = in.readLine();
            //On va lire ligne par ligne en partant de la fin
            //En pratique 5-6 lecture de ligne avant de retrouver le >fragment
            while(line != null) {
                String[] tab = line.split(" ");
                if (tab[0].equals(">fragment")) {
                    in.close();
                    return Integer.parseInt(tab[1].substring(0, tab[1].length()-1));
                }line = in.readLine();
            }in.close();return 0;
        }catch(FileNotFoundException e){
            System.out.println("Fichier non trouvé lors de la lecture");
        }catch(IOException e){
            System.out.println("Impossible d'ouvrir le fichier");
        }return 0;
    }
    /**
     * Extrait la poisition et le fragment issu d'un token de lecture de fichier
     * @param token le token lu sur le fichier fasta
     * @return un Tuple contenant la position+1 du fragment et le fragment
     */
    private Tuple extractToken(String token){
        String[] fragment = token.split("\n");
        String[] head = fragment[0].split(" ");
        if (this.collection < 0)
            this.collection = Integer.parseInt(head[3]);
        int index = Integer.parseInt(head[1].substring(0, head[1].length()-1));
        String s = "";
        for(int i = 1; i<fragment.length; i++){
            s = s.concat(fragment[i]);
        }

        return new Tuple(index, toProteineMatrix(s));
    }

    public int getCollection(){
        return this.collection;
    }

    /*
    private Tuple extractToken(String token){
        String[] fragment = token.split("\n");
        String[] head = fragment[0].split(" ");
        int index = Integer.parseInt(head[1].substring(0, head[1].length()-1));
        String s = "";
        for(int i = 1; i<fragment.length; i++){
            s = s.concat(fragment[i]);
        }

        return new Tuple(index, toBitSetMatrix(s));
    }
     */

    /**
     * Converti un caractère en BitSet
     * @param c le caractère à convertir
     * @return le bitset correspondant
     */
    private static Proteine char2bit(String c){
        Proteine bitSetCharToAdd;
        switch(c.toLowerCase()){
            case "a"://1
                bitSetCharToAdd = new Proteine(1);
                break;
            case "t": //2
                bitSetCharToAdd = new Proteine(2);
                break;
            case "g": //3
                bitSetCharToAdd = new Proteine(3);
                break;
            case "c": //4
                bitSetCharToAdd = new Proteine(4);
                break;
            default://0 le gap si pas trouvé de lettre debug case
                bitSetCharToAdd = new Proteine(0);
        }
        return bitSetCharToAdd;
    }
    /*
    private static BitSet char2bit(String c){
        BitSet bitSetCharToAdd = new BitSet(3);
        switch(c.toLowerCase()){
            case "a"://001
                bitSetCharToAdd.set(2);
                break;
            case "c": //110
                bitSetCharToAdd.set(0);
                bitSetCharToAdd.set(1);
                break;
            case "g": //010
                bitSetCharToAdd.set(1);
                break;
            case "t": //101
                bitSetCharToAdd.set(0);
                bitSetCharToAdd.set(2);
                break;
            default://111 le gap si pas trouvé de lettre debug case
                bitSetCharToAdd.set(0);
                bitSetCharToAdd.set(1);
                bitSetCharToAdd.set(2);
        }return bitSetCharToAdd;
    }*/


    public static Fragment toProteineMatrix(String s) {
        Proteine[] b = new Proteine[s.length()];
        //todo voir si pas possible de réellement instaurer les char
        String[] charArray = s.split("");
        for(int i = 0; i < charArray.length; i++){
            b[i] = char2bit(charArray[i]);
        }
        return new Fragment(b);
    }

    /**
     * Converti un string en tableau de BitSet (Fragment) de taille 3
     * @param s
     * @return
     */
    /*
    public static Fragment toBitSetMatrix(String s){
        BitSet[] b = new BitSet[s.length()];
        //todo voir si pas possible de réellement instaurer les char
        String[] charArray = s.split("");
        for(int i = 0; i < charArray.length; i++){
            b[i] = char2bit(charArray[i]);
        }
        return new Fragment(b);
    }*/

    /**
     * Converti un string en un grand BitSet de taille 3*la taille de s
     * @param s
     * @return
     */
    @Deprecated
    private BitSet toBitSet(String s){
        BitSet b = new BitSet(s.length()*3);
        String[] charArray = s.split("");
        for(int i = 0; i < charArray.length; i++){
            switch(charArray[i]){
                case "a"://001
                    b.set(3*i+2);
                    break;
                case "c": //110
                    b.set(3*i);
                    b.set(3*i+1);
                    break;
                case "g": //010
                    b.set(3*i+1);
                    break;
                case "t": //101
                    b.set(3*i);
                    b.set(3*i+2);
                    break;
                default://111 le gap si pas trouvé de lettre debug case
                    b.set(3*i);
                    b.set(3*i+1);
                    b.set(3*i+2);
            }
        }
        return b;
    }

}
