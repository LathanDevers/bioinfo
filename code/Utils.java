import java.util.BitSet;
class Arco{
    int n,m;
    int p;
    char j,k;
    public Arco(int n,int m,int p,char j, char k){
        this.n=n;
        this.m=m;
        this.p=p;
        this.j=j;
        this.k=k;
    }
}

class Libr{
    int length;
    Arco[] liste;
    public Libr(int length){
        this.length=0;
        liste=new Arco[length];
    }
    public Libr(Arco[] liste, int length){
        this.liste=liste;
        this.length=length;
    }
    public void addArco(Arco a){
        if (this.length<this.liste.length) {
            this.liste[this.length]=a;
            this.length++;
        }
    }
}

/*class ComputeWeightThread extends Thread{

    private int maximum;
    private Adj adjacenceMatrix;
    private Char a;

    public computeWeightThread(int maximum,Adj adjacenceMatrix,char a){
        this.maximum=maximum;
        this.adjacenceMatrix=adjacenceMatrix;
        this.a=a;
    }

    public void run(){
        if (this.a=='m') {
            for (int i = 0; i < this.adjacenceMatrix.getM()-1; i++) {
                if(this.maximum<this.adjacenceMatrix.get(this.adjacenceMatrix.getN()-1,i)){
                    this.maximum=this.adjacenceMatrix.get(this.adjacenceMatrix.getN()-1,i);
                }
            }
        }else if(this.a=='n'){
            for (int i = 0; i < this.adjacenceMatrix.getN()-1; i++) {
                if(this.maximum<this.adjacenceMatrix.get(i,this.adjacenceMatrix.getM()-1)){
                    this.maximum=this.adjacenceMatrix.get(i,this.adjacenceMatrix.getM()-1);
                }
            }
        }
    }
}*/
/*
class Utils{
    Graph g;
    Node[][] e;
    int[][] e1;
    Arco[] arcs;
    Libr arco;

    public Utils(Graph g){
        this.g=g;
        e=new Node[1][g.nodes.length];
        e1=new int[1][g.nodes.length];
        for (int i = 0; i < g.nodes.length; i++) {
            e[0][i]=g.nodes[i];
            e1[0][i]=i;
        }
        this.arcs=new Arco[g.arcs.length*4];
        for (int i = 0; i < g.arcs.length; i++) {
            arcs[i*4]=new Arco(g.arcs[i].par[0],g.arcs[i].par[1],g.arcs[i].pff,'f','f');
            arcs[i*4+1]=new Arco(g.arcs[i].par[0],g.arcs[i].par[1],g.arcs[i].pfc,'f','c');
            arcs[i*4+2]=new Arco(g.arcs[i].par[0],g.arcs[i].par[1],g.arcs[i].pcf,'c','f');
            arcs[i*4+3]=new Arco(g.arcs[i].par[0],g.arcs[i].par[1],g.arcs[i].pcc,'c','c');
        }
        this.arco=new Libr(arcs,arcs.length);
    }
    public Libr deleteArcs(Arco a){
        Arco[] temp=new Arco[this.arco.length];
        int l=0;
        for (int i = 0; i < this.arco.length; i++) {
            if (this.arco.liste[i].k==a.j || this.arco.liste[i].j==a.k) {
                temp[l]=this.arcs[i];
                l++;
            }
        }
        Libr res=new Libr(temp,l);
        return res;
    }
    public Libr Greedy(){
        int n=this.g.nodes.length;
        int[] in=new int[n];
        int[] out=new int[n];
        for(int i=0;i<n;i++){
            in[i]=0;
            out[i]=0;
            //makeset(i);   //   e1
        }

        tri(this.arco);

        boolean b=true;
        Libr arcs_choisis=new Libr(this.g.nodes.length);

        while(b){
            if(in[g]==0 && out[f]==0 && findset(f)!=findset(g)){
                select(f,g);
                in[g]=1;
                out[f]=1;
                union(findset(f),findset(g));
            }
            if(e1[0].length==1){
                b=false;
            }
        }
        return arcs_choisis;
    }

    /*public void makeset(int i){
        
    }*/
/*
    public void findset(f){

    }

    public void union(e1,e2){

    }

    public void select(f,g){
        
    }

    public void triFusion(Arco tableau[],int length)
        {
        int longueur=length;
        if (longueur>0)
            {
            triFusion(tableau,0,longueur-1);
            }
        }

    private void triFusion(Arco tableau[],int deb,int fin)
        {
        if (deb!=fin)
            {
            int milieu=(fin+deb)/2;
            triFusion(tableau,deb,milieu);
            triFusion(tableau,milieu+1,fin);
            fusion(tableau,deb,milieu,fin);
            }
        }

    private void fusion(Arco tableau[],int deb1,int fin1,int fin2)
        {
        int deb2=fin1+1;

        //on recopie les éléments du début du tableau
        Arco table1[]=new Arco[fin1-deb1+1];
        for(int i=deb1;i<=fin1;i++)
            {
            table1[i-deb1]=tableau[i];
            }
        
        int compt1=deb1;
        int compt2=deb2;
        
        for(int i=deb1;i<=fin2;i++)
            {        
            if (compt1==deb2) //c'est que tous les éléments du premier tableau ont été utilisés
                {
                break; //tous les éléments ont donc été classés
                }
            else if (compt2==(fin2+1)) //c'est que tous les éléments du second tableau ont été utilisés
                {
                tableau[i]=table1[compt1-deb1]; //on ajoute les éléments restants du premier tableau
                compt1++;
                }
            else if (table1[compt1-deb1].p<tableau[compt2].p)
                {
                tableau[i]=table1[compt1-deb1]; //on ajoute un élément du premier tableau
                compt1++;
                }
            else
                {
                tableau[i]=tableau[compt2]; //on ajoute un élément du second tableau
                compt2++;
                }
            }
        }

}*/
