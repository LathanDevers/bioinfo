import java.io.File;
import java.io.FileWriter;
import java.util.LinkedList;

class Main{
    
    public Main(){

    }
    //représentation sur 5 valeurs différentes a la place de strings

    public static void main(String[] args) {








        // String s = "attagaccatgcggc";
        // String t = "atcggcattcagt";
        //Semi algo = new Semi(s,t);

        //System.out.println(algo.sm.toString());

        // String[] f={"ATGC","TGCAT","GCC"};
        /*
        String[] c=Graph.setCFragments(f);
        for(int i=0;i<c.length;i++){
            System.out.println(c[i]);
            System.out.println(f[i]);
        }*/

        //Graph g=new Graph(f);
        /*for (int i = 0; i < g.nodes.length; i++) {
            System.out.println(g.nodes[i]);
        }
        for (int i = 0; i < g.arcs.length; i++) {
            System.out.println(g.arcs[i].toString());
        }*/
        //SemiV2 sv2=new SemiV2(s,t);

        // Fragment f1=FragParser.toProteineMatrix(s);
        // Fragment f2=FragParser.toProteineMatrix(t);

        // Alignment a=SemiV2.computAlignment(f1, f2);

        // System.out.println(a);

        // Consensus c = new Consensus(f);

        // for (int i = 0; i < c.llf.length; i++) {
        //     for (int j = 0; j < c.llf[i].size(); j++) {
        //         System.out.print(c.llf[i].get(j));
        //     }
        //     System.out.print("\n");
        // }

        //System.out.println(a);
        //SemiV2 sv2=new SemiV2(f1, f2);



        // System.out.println(ow);
        LinkedList<Alignment> ow=new LinkedList<>();
        // f: tatgc
        // g: -at-c
        // ---------
        // g: a-tc
        // h: aatca
        // String[] s1={"TATGC",
        //              "-AT-C"};
        // String[] s2={"A-TC",
        //              "AATCA"};
        // String[] s3={"CTAC--",
        //              "--ACAT"};
        // String[] s4={"ACAT--",
        //              "---TAA"};
        // ow.addLast(s1);
        // ow.addLast(s2);
        // ow.addLast(s3);
        // ow.addLast(s4);

        /*
        AGCAA-------
        --CAATG-----
        --CAATG-----
        -----TGGGA--
        -----TGGGA--
        --------GATC
        ============
        AGCAATGGGATC
         */

        // geta("TATGC", "-AT-C");
        // geta("A-TC", "AATCA");
        //LinkedList<Alignment> ow=new LinkedList<>();
        //ow.addLast(geta("AGCAA--", "--CAATC"));
        //ow.addLast(geta("CAATG---", "---TGGGA"));
        //ow.addLast(geta("TGGGA", "---GATC"));


        /*
        a1 = AL:
        --AATCCCGG--------
        ------CCGCTAGGCA--

        ------CCGCTAGGCA---
        CCCCGATCG-TAG-CAT--

        CCCCGATCG-TAG-CAT--
        ----------TAG-CATGC
        ====================
        CCCCGACCGCTAGGCATGC
        vs
        AATGCCCGGTGGTAGCATGC
         */

        //GAGGCGAGCGTATGTTATCTGATGGTGTCGTTCGCCCGAGAAAGCTTATCTGAGC
        //GAGGCGAGCCTATGTATCAAATCTGATGGTGTCGAACGCCCCAAAAAGCTTATCTGAGC


        /*
        a1 = AL:
        GAGGCGAGCGTA---
        ----CGAGGCTATGT

        a2 = AL:
        CGAGGCTATGT--------------
        ----------TATCAAATCTGATGG

        a3 = AL:
        TATCAAATCTGATGG------------
        -----------ATGGTGTCGAACGCCC

        GAGGCGAGCGTA---
        ----CGAGGCTATGT
        --------------TATCAAATCTGATGG
        -------------------------ATGGTGTCGAACGCCC
        ----------------------------------AA-GCCCCGAGAAAGCT
        -----------------------------------------GAGAAAGCTTATCTGA
        ----------------------------------------------AGCTTCTCTGAGC
         */

        //GAGTCTCCGCA
        //GAGTCTCCGGA

        Alignment a1 = SemiV2.computAlignment(new Fragment("GAGGCGAGCGTA"), new Fragment("CGAGGCTATGT"));
        Alignment a2 = SemiV2.computAlignment(new Fragment("CGAGGCTATGT"), new Fragment("TATCAAATCTGATGG"));
        Alignment a3 = SemiV2.computAlignment(new Fragment("TATCAAATCTGATGG"), new Fragment("ATGGTGTCGAACGCCC"));
        Alignment a4 = SemiV2.computAlignment(new Fragment("ATGGTGTCGAACGCCC"), new Fragment("AAGCCCCGAGAAAGCT"));
        Alignment a5 = SemiV2.computAlignment(new Fragment("AAGCCCCGAGAAAGCT"), new Fragment("GAGAAAGCTTATCTGA"));
        Alignment a6 = SemiV2.computAlignment(new Fragment("GAGAAAGCTTATCTGA"), new Fragment("AGCTTCTCTGAGC"));


        System.out.println("a1 = " + a1);
        System.out.println("a2 = " + a2);
        System.out.println("a2 = " + a3);
        System.out.println("a3 = " + a4);
        System.out.println("a4 = " + a5);
        System.out.println("a5 = " + a6);
        //System.out.println("a6 = " + a3);
        //System.out.println("a4 = " + a4);
        ow.addLast(a1);
        ow.addLast(a2);
        ow.addLast(a3);
        ow.addLast(a4);
        ow.addLast(a5);
        ow.addLast(a6);

        Consensus c=new Consensus(ow);
        String k=c.computeSupersequence(ow);

        System.out.println(c.kappa());
        System.out.println(k);
        // System.out.println("CCCCGACCGCTAGGCATGC <== cible");
        // System.out.println(k.equals("CCCCGACCGCTAGGCATGC"));
        // System.out.println(k.equals("AGCAATGGGATC"));

        try{
            File f=new File("../result");
            FileWriter fw=new FileWriter(f);
            fw.write(k);
            fw.close();
            System.out.println("The sequence has been saved in "+f.getAbsolutePath());
        }catch(Exception e){}
    }

    public static Alignment geta(String s1,String s2){
        return new Alignment(0,new Fragment(s1),new Fragment(s2));
    }
}