import java.util.BitSet;

class Tuple{
    
    int pos;
    //BitSet[] frag;
    Fragment frag;
    public Tuple(int pos, Fragment frag){
        this.pos = pos;
        this.frag = frag;
    }

    int getPos() {
        return pos;
    }

    Fragment getFrag() {
        return frag;
    }

    @Override
    public String toString(){
        return frag.toString();
    }

}