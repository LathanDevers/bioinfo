public enum ArcType {

    F2G, G2F, FP2G, G2FP, F2GP, GP2F, FP2GP, GP2FP

}