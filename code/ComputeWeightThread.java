/**
 * ComputeWeightThread
 * @version 1
 * @author Daniels M.
 */
class ComputeWeightThread implements Runnable{

        private int maximum;
        private Adj adjacenceMatrix;
        private char a;
        private int x,y;

        public ComputeWeightThread(Adj adjacenceMatrix,char a){
            this.adjacenceMatrix=adjacenceMatrix;
            this.a=a;
            this.maximum=Integer.MIN_VALUE; 
        }
    
        public void run(){
            if (this.a=='n') {
                for (int i = 0; i < this.adjacenceMatrix.getM(); i++) {
                    if(this.maximum<this.adjacenceMatrix.get(this.adjacenceMatrix.getN()-1,i)){
                        this.maximum=this.adjacenceMatrix.get(this.adjacenceMatrix.getN()-1,i);
                        this.x=this.adjacenceMatrix.getN()-1;
                        this.y=i;
                    }
                }
            }else if(this.a=='m'){
                for (int i = 0; i < this.adjacenceMatrix.getN(); i++) {
                    if(this.maximum<this.adjacenceMatrix.get(i,this.adjacenceMatrix.getM()-1)){
                        this.maximum=this.adjacenceMatrix.get(i,this.adjacenceMatrix.getM()-1);
                        this.x=i;
                        this.y=this.adjacenceMatrix.getM()-1;
                    }
                }
            }
            //System.out.println("compute_weight "+this.x+" "+this.y+" "+this.maximum);

        }

        public int getMaximum(){
            return this.maximum;
        }

        public Adj getAdjacenceMatrix(){
            return this.adjacenceMatrix;
        }

        public char getA(){
            return this.a;
        }

        public int[] getPosition(){
            int[] position={this.x,this.y};
            return position;

        }
    }