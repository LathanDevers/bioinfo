/**
 * Classe Semi
 * @author Daniels M.
 * @version 2
 */
class SemiV2 {
    public final int GAPSCORE = -2;

    private Fragment fragment1, fragment2;
    private Adj adjacenceMatrixFragmentToFragment, adjacenceMatrixFragmentToInvComp;
    private int[] weights;
    private boolean[] factors;
    private int[][] positions;
    private Alignment[] alignments;

     private int maxCol;
     private int maxRow;

     private int maxColInv;
     private int maxRowInv;

     private boolean computeF1;
     private boolean computeF2;

     /**
     * Algorithme semi-global de matching (constructeur)
     * @param fragment1 (Fragment)
     * @param fragment2 (Fragment)
     */
    /*
    public SemiV2(Fragment fragment1, Fragment fragment2){
        this.fragment1=fragment1;
        this.fragment2=fragment2;
        this.adjacenceMatrixFragmentToFragment=new Adj(this.fragment1,this.fragment2);
        this.adjacenceMatrixFragmentToInvComp=new Adj(this.fragment1,this.fragment2.getInvComp());
        Thread computeAdjacenceMatrix1=new ComputeAdjacenceMatrixThread(this.fragment1, this.fragment2,this.adjacenceMatrixFragmentToFragment);
        Thread computeAdjacenceMatrix2=new ComputeAdjacenceMatrixThread(this.fragment1, this.fragment2.getInvComp(),this.adjacenceMatrixFragmentToInvComp);
        computeAdjacenceMatrix1.start();
        computeAdjacenceMatrix2.start();
        this.positions=new int[8][2];
        try{
            computeAdjacenceMatrix1.join();
            computeAdjacenceMatrix2.join();
        }catch (Exception e){}
        this.weights=computeWeights();
        this.alignments=reconstruct();
    }*/

    public SemiV2(Fragment fragment1, Fragment fragment2){
        this(fragment1, fragment2, true, true);
    }
    public SemiV2(Fragment fragment1, Fragment fragment2, boolean computeF1, boolean computeF2){
        this.fragment1=fragment1;
        this.fragment2=fragment2;
        this.computeF1 = computeF1;
        this.computeF2 = computeF2;
        this.adjacenceMatrixFragmentToFragment=new Adj(this.fragment1, this.fragment2);
        this.adjacenceMatrixFragmentToInvComp=new Adj(this.fragment1, this.fragment2.getInvComp());
        this.positions=new int[4][2];
        computeAdjMatrix();
        this.weights=computeWeights();
        //this.alignments=reconstruct();
        this.factors=new boolean[8];
        //computeFactors();
    }

    public static Alignment computAlignment(Fragment fragment1,Fragment fragment2){

        Adj adjacenceMatrix=new Adj(fragment1,fragment2);
        int positionX=0,positionY=0;
        int max=-Integer.MAX_VALUE;

        for (int i = 1; i < adjacenceMatrix.getN(); i++) {
            for (int j = 1; j < adjacenceMatrix.getM(); j++) {
                int toAdd = Math.max(
                        adjacenceMatrix.get(i, j - 1) - 2,
                        Math.max(
                                adjacenceMatrix.get(i - 1, j) - 2,
                                adjacenceMatrix.get(i - 1, j - 1) + SemiV2.pScore(fragment1, fragment2,
                                        i - 1, j - 1)));

                if (i == adjacenceMatrix.getN() - 1 && toAdd > max) {
                    max = toAdd;
                    positionX = i;
                    positionY = j;
                }
                if (j == adjacenceMatrix.getM() - 1 && toAdd > max) {
                    max = toAdd;
                    positionX = i;
                    positionY = j;
                    // max=0;
                }
                adjacenceMatrix.set(i, j, toAdd);
            }
        }
        if (positionY==adjacenceMatrix.getM() - 1) {
            max=-Integer.MAX_VALUE;
        }

        ComputeAlignmentThread computeAlignmentThread = new ComputeAlignmentThread(max, fragment1, fragment2, adjacenceMatrix, new int[]{positionX,positionY});

        Alignment result=computeAlignmentThread.getAlignment();

        return result;
    }



    /**
     * @return Les fragments d'entrée (Fragment)
     */
    public Fragment[] getFragments(){
        Fragment [] result={this.fragment1,this.fragment2};
        return result;
    }

    public void computeAdjMatrix(){

        this.maxCol=0;
        this.maxRow=0;
        this.maxColInv=0;
        this.maxRowInv=0;

        int N = adjacenceMatrixFragmentToFragment.getN();
        int M = adjacenceMatrixFragmentToFragment.getM();
        //System.out.println("N et M = " + N + "/" + M);
        if (computeF1) {
            for (int i = 1; i < N; i++) {
                for (int j = 1; j < M; j++) {
                    int toAdd = Math.max(
                            adjacenceMatrixFragmentToFragment.get(i, j - 1) + GAPSCORE,
                            Math.max(
                                    adjacenceMatrixFragmentToFragment.get(i - 1, j) + GAPSCORE,
                                    adjacenceMatrixFragmentToFragment.get(i - 1, j - 1) + SemiV2.pScore(this.fragment1, this.fragment2,
                                            i - 1, j - 1)));

                    if (i == adjacenceMatrixFragmentToFragment.getN() - 1 && toAdd > this.maxRow) {
                        this.maxRow = toAdd;
                        this.positions[0][0] = i;
                        this.positions[0][1] = j;
                    }
                    if (j == this.adjacenceMatrixFragmentToFragment.getM() - 1 && toAdd > this.maxCol) {
                        this.maxCol = toAdd;
                        this.positions[1][0] = i;
                        this.positions[1][1] = j;
                    }
                    this.adjacenceMatrixFragmentToFragment.set(i, j, toAdd);
                }
            }
        }
        if (computeF2) {
            for (int i = 1; i < this.adjacenceMatrixFragmentToInvComp.getN(); i++) {
                for (int j = 1; j < this.adjacenceMatrixFragmentToInvComp.getM(); j++) {
                    int toAdd = Math.max(
                            this.adjacenceMatrixFragmentToInvComp.get(i,j-1)+GAPSCORE,
                            Math.max(
                                    this.adjacenceMatrixFragmentToInvComp.get(i-1,j)+GAPSCORE,
                                    this.adjacenceMatrixFragmentToInvComp.get(i-1,j-1)+SemiV2.pScore(this.fragment1,this.fragment2.getInvComp(),
                                            i-1,j-1/* character dans fragment1 i-1, character dans fragment2 j-1 */)
                            )
                    );
                    if (i == this.adjacenceMatrixFragmentToInvComp.getN()-1 && toAdd > this.maxRowInv){
                        this.maxRowInv = toAdd;
                        this.positions[2][0]=i;
                        this.positions[2][1]=j;
                    }
                    if (j == this.adjacenceMatrixFragmentToInvComp.getM()-1 && toAdd > this.maxColInv){
                        this.maxColInv = toAdd;
                        this.positions[3][0]=i;
                        this.positions[3][1]=j;
                    }
                    this.adjacenceMatrixFragmentToInvComp.set(i,j, toAdd);
                }
            }
        }
    }
    /**
     * @return Les matrice d'adjacence (Adj)
     */
    public Adj[] getAdjaceneMatrixes(){
        Adj[] result={this.adjacenceMatrixFragmentToFragment,this.adjacenceMatrixFragmentToInvComp};
        return result;
    }

    /**
     * @return Les poids dans l'ordre (int):
     *      0=f->g
     *      1=f->g'
     *      2=f'->g
     *      3=f'->g'
     *      4=g->f
     *      5=g->f'
     *      6=g'->f
     *      7=g'->f'
     */
    public int[] getWeights(){
        return this.weights;
    }

    public Alignment[] getAlignments(){
        return this.alignments;
    }

    /**
     * Calcule le pScore
     * 
     * @param fragment1 (Fragment)
     * @param fragment2 (Fragment)
     * @param index1 (int)
     * @param index2 (int)
     * 
     * @return +1 ou -1 (match ou pas match)
     */
    public static int pScore(Fragment fragment1, Fragment fragment2, int index1, int index2){
        return fragment1.equals(index1  ,fragment2,index2,1);
    }

    /**
     * Calcule les poids des alignements
     * 
     * @return int[] result:
     *      0=f->g
     *      1=f->g'
     *      2=f'->g
     *      3=f'->g'
     *      4=g->f
     *      5=g->f'
     *      6=g'->f
     *      7=g'->f'
     *//*
    private int[] computeWeights(){
        int[] result=new int[8];
        // this.positions
        //int x1=0,x2=0,y1=0,y2=0;
        //int[] m1=computeWeight(this.adjacenceMatrixFragmentToFragment,x1,y1,x2,y2);
        //int[] m2=computeWeight(this.adjacenceMatrixFragmentToInvComp,this.positions[6][0],this.positions[6][1],this.positions[1][0],this.positions[1][1]);

        int[] m1={0,0};
        int[] m2={0,0};

        ComputeWeightThread computeWeight1Thread=new ComputeWeightThread(this.adjacenceMatrixFragmentToFragment, 'm');

        ComputeWeightThread computeWeight2Thread=new ComputeWeightThread(this.adjacenceMatrixFragmentToFragment, 'n');

        ComputeWeightThread computeWeight3Thread=new ComputeWeightThread(this.adjacenceMatrixFragmentToInvComp, 'm');

        ComputeWeightThread computeWeight4Thread=new ComputeWeightThread(this.adjacenceMatrixFragmentToInvComp, 'n');

        Thread cw1t=new Thread(computeWeight1Thread);
        Thread cw2t=new Thread(computeWeight2Thread);
        Thread cw3t=new Thread(computeWeight3Thread);
        Thread cw4t=new Thread(computeWeight4Thread);

        cw1t.start();
        cw2t.start();
        cw3t.start();
        cw4t.start();

        try{
            cw1t.join();
            cw2t.join();
            cw3t.join();
            cw4t.join();
        }catch(Exception e){}

        this.positions[4]=computeWeight1Thread.getPosition();
        this.positions[0]=computeWeight2Thread.getPosition();
        this.positions[6]=computeWeight3Thread.getPosition();
        this.positions[1]=computeWeight4Thread.getPosition();

        m1[0]=computeWeight1Thread.getMaximum();
        m1[1]=computeWeight2Thread.getMaximum();
        m2[0]=computeWeight3Thread.getMaximum();
        m2[1]=computeWeight4Thread.getMaximum();

        this.positions[3][0]=this.positions[4][1];
        this.positions[3][1]=this.positions[4][0];
        this.positions[7][0]=this.positions[0][1];
        this.positions[7][1]=this.positions[0][0];
        this.positions[2][0]=this.positions[6][1];
        this.positions[2][1]=this.positions[6][0];
        this.positions[5][0]=this.positions[1][1];
        this.positions[5][1]=this.positions[1][0];

        result[0]=m1[1];
        result[1]=m2[1];
        result[2]=m2[0];
        result[3]=m1[0];
        result[4]=m1[0];
        result[5]=m2[1];
        result[6]=m2[0];
        result[7]=m1[1];

        return result;
    }*/
    public int computeWeight(char a, char b) {
        if (b == 'g') {
            if (a == 'n')
                return maxRow;
            else
                return maxCol;
        }else
            if (a == 'n')
                return maxRowInv;
            else
                return maxColInv;
    }

    public int[] computeWeights(){
        int[] result=new int[8];
        // this.positions
        //int x1=0,x2=0,y1=0,y2=0;
        //int[] m1=computeWeight(this.adjacenceMatrixFragmentToFragment,x1,y1,x2,y2);
        //int[] m2=computeWeight(this.adjacenceMatrixFragmentToInvComp,this.positions[6][0],this.positions[6][1],this.positions[1][0],this.positions[1][1]);

        int[] m1={0,0};
        int[] m2={0,0};

        m1[0]=computeWeight('m','g');
        m1[1]=computeWeight('n','g');
        m2[0]=computeWeight('m','r');
        m2[1]=computeWeight('n','r');

        result[0]=m1[1];
        result[1]=m2[1];
        result[2]=m2[0];
        result[3]=m1[0];
        result[4]=m1[0];
        result[5]=m2[1];
        result[6]=m2[0];
        result[7]=m1[1];

        return result;
    }

    private void computeFactors(){
        for (int i = 0; i < this.factors.length; i++) {
            if (this.weights[i]>=0)
                this.factors[i]=false;
            else
                this.factors[i]=true;
        }
    }

    public boolean factor(int i){
        return this.factors[i];
    }

    /**
     *
     * @return
     */
    public Alignment[] reconstruct(){

        Alignment[] result=new Alignment[8];
        if (computeF1) {
            ComputeAlignmentThread computeAlignmentThread1 = new ComputeAlignmentThread(this.weights[0], this.fragment1, this.fragment2, this.adjacenceMatrixFragmentToFragment, this.positions[0]);
            ComputeAlignmentThread computeAlignmentThread2 = new ComputeAlignmentThread(this.weights[4], this.fragment1, this.fragment2, this.adjacenceMatrixFragmentToFragment, this.positions[1]);

            result[0] = computeAlignmentThread1.getAlignment();
            result[4] = computeAlignmentThread2.getAlignment();
        }

        if (computeF2) {
            ComputeAlignmentThread computeAlignmentThread3=new ComputeAlignmentThread(this.weights[6], this.fragment1, this.fragment2.getInvComp(), this.adjacenceMatrixFragmentToInvComp, this.positions[3]);
            ComputeAlignmentThread computeAlignmentThread4=new ComputeAlignmentThread(this.weights[1], this.fragment1, this.fragment2.getInvComp(), this.adjacenceMatrixFragmentToInvComp, this.positions[2]);
            result[6] = computeAlignmentThread3.getAlignment();
            result[1] = computeAlignmentThread4.getAlignment();
        }

        // System.out.println("F->G : "+result[0].poids+" "+this.weights[0]);
        // System.out.println(result[0].fragment1.toString());
        // System.out.println(result[0].fragment2.toString());

        // System.out.println("G->F : "+result[4].poids+" "+this.weights[4]);
        // System.out.println(result[4].fragment2.toString());
        // System.out.println(result[4].fragment1.toString());

        // System.out.println("F->G' : "+result[1].poids);
        // System.out.println(result[1].fragment1.toString());
        // System.out.println(result[1].fragment2.toString());

        // System.out.println("G'->F : "+result[6].poids);
        // System.out.println(result[6].fragment2.toString());
        // System.out.println(result[6].fragment1.toString());

        // System.out.println("G'->F' : "+result[0].poids);
        // System.out.println(result[0].fragment2.getInvComp().toString());
        // System.out.println(result[0].fragment1.getInvComp().toString());

        // System.out.println("F'->G' : "+result[4].poids+" "+this.weights[4]);
        // System.out.println(result[4].fragment1.getInvComp().toString());
        // System.out.println(result[4].fragment2.getInvComp().toString());

        // System.out.println("G->F' : "+result[1].poids);
        // System.out.println(result[1].fragment2.getInvComp().toString());
        // System.out.println(result[1].fragment1.getInvComp().toString());

        // System.out.println("F'->G : "+result[6].poids);
        // System.out.println(result[6].fragment1.getInvComp().toString());
        // System.out.println(result[6].fragment2.getInvComp().toString());
 
        return result;
    }
}
