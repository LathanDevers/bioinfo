/**
 * Classe d'alignement
 * @version 1
 * @author Daniels M.
 */
public class Alignment{
    int poids;
    Fragment fragment1,fragment2;

    public Alignment(){};
     /**
      * 
      * @param poids
      * @param fragment1
      * @param fragment2
      */
    public Alignment(int poids,Fragment fragment1,Fragment fragment2){
        this.poids=poids;
        this.fragment1=fragment1;
        this.fragment2=fragment2;
    }

    public void setF1(Fragment fragment){
        this.fragment1=fragment;
    }

    public void setF2(Fragment fragment){
        this.fragment2=fragment;
    }

    @Override
    public String toString() {
        return  "AL:\n"+fragment1 + "\n" + fragment2 + "\n";
    }
}
