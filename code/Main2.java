import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.BitSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

class Main2{
    public static void main(String[] args){

        if (args.length == 5){
            String filename = args[0];
            String option1 = args[1];
            String value1 = args[2];
            String option2 = args[3];
            String value2 = args[4];

            String output, output_ic;

            if (option1.equals("-out")){
                output = value1;
                output_ic = value2;
            }else{
                output = value2;
                output_ic = value1;
            }

            FragParser fp = new FragParser(filename);
            //int collection = fp.getCollection();
            long t1 = System.currentTimeMillis();
            Graph g = new Graph(fp.getFrags());
            long t2 = System.currentTimeMillis();
            System.out.println("Temps de construction de l'Overlap MutliGraph = " + ((t2-t1)/1000.)+"s");
            LinkedList<Alignment> a = g.runGreedy(false);
            long t3 = System.currentTimeMillis();
            System.out.println("Temps d'éxécution de Greedy = " + ((t3-t2)/1000.)+"s");
            Consensus c=new Consensus(a);
            String k=c.computeSupersequence(a);
            try{
                write(output, fp.getCollection(), k);
            }catch(Exception e){
                System.err.println("Le fichier a mal été écris.");
            }
            Fragment cible = new Fragment(k);
            try{
                write(output_ic, fp.getCollection(), cible.getInvComp().toString());
            }catch(Exception e){
                System.err.println("Le fichier a mal été écris.");
            }
        }
        else {
            System.err.println("Veuillez entrer un fichier de sortie pour le fichier cible et son complémentaire inversés ainsi qu'un fichier d'entrée");

        }
    }

    private static void write(String output, int collection, String cible) throws IOException {
        File f=new File(output);
        FileWriter fw=new FileWriter(f);
        fw.write("> Groupe-5 Collection " + collection + " Longueur " + cible.length() + "\n");
        int i = 0;
        while(i+80<cible.length()){
            fw.write(cible.substring(i, i+80)+"\n");
            i+=80;
        }
        fw.write(cible.substring(i));
        fw.close();
        System.out.println("The sequence has been saved in "+f.getAbsolutePath());
    }
}