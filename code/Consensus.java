import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingDeque;

class Consensus{
    
    LinkedList<LinkedList<Character>> llf;
    int pos_prec;
    int lm;
    int[] starts;
    
    public Consensus(LinkedList<Alignment> alignments){
        this.llf=new LinkedList<LinkedList<Character>>();
        starts=new int[alignments.size()*2];
        String[] alignment={alignments.get(0).fragment1.toString(),alignments.get(0).fragment2.toString()};
        initialisation(alignment);
    }

    public String computeSupersequence(LinkedList<Alignment> alignments){
        long t1 = System.currentTimeMillis();
        String result="";
        //System.out.println("taille alignments : "+alignments.size());
        for (int i = 1; i < alignments.size(); i++) {
            this.addAlignmentV2(alignments.get(i));
            // for (int j = 0; j < this.llf.size(); j++) {
            //     System.out.println(this.starts[j]+"\t"+this.llf.get(j));
            // }
            //System.out.println("/>/>/>/>/>/>/>/>/>/>/>/>/>/");
            // System.out.print(this.lm+"/"+(alignments.size()+1)+"\r");
        }

        String tempString=supersequencage();

        for (int i = 0; i < tempString.length(); i++) {
            if (tempString.charAt(i)!='-') {
                result+=tempString.charAt(i);
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println("Propagation des gaps et consensus: " + ((t2-t1)/1000) + "s");
        return result;
    }

    private void initialisation(String[] alignment){
        this.pos_prec=0;
        this.lm=0;
        starts[this.lm]=this.pos_prec;
        this.llf.add(new LinkedList<Character>());
        for (int i = 0; i < alignment[this.lm].length(); i++) {
            this.llf.get(this.lm).addLast(alignment[this.lm].charAt(i));
        }
        this.lm++;

        this.llf.add(new LinkedList<Character>());
        for (int i = 0; i < alignment[this.lm].length(); i++) {
            this.llf.get(this.lm).addLast(alignment[this.lm].charAt(i));
        }
        // for (int j = 0; j < this.llf.size(); j++) {
        //     System.out.println(this.starts[j]+"\t"+this.llf.get(j));
        // }
        // System.out.println("/>/>/>/>/>/>/>/>/>/>/>/>/>/");
        this.lm++;
        // System.out.println(this.lm);
    }

    private Character getLetter(int[] max){
        int index=0;
        int maxValue=max[0];
        int d=0;

        for (int i = 0; i < max.length-1; i++) {
            if(max[i]==0){
                d++;
            }
            if (maxValue<max[i]) {
                maxValue=max[i];
                index=i;
            }
        }
        if (d<4) {
            switch (index) {
                case 0:
                    return 'A';
                case 1:
                    return 'C';
                case 2:
                    return 'T';
                default:
                    return 'G';
            }
        }else{
            return '-';
        }
    }

    public String supersequencage(){
        String result="";
        int[] max;
        max=new int[5];
        boolean end=true;
        int i=0;
        boolean save=false;
        while(end) {
            // System.out.println("Loading Consensus: " + i/this.llf.get(this.llf.size()-1).size());
            max[0]=0;
            max[1]=0;
            max[2]=0;
            max[3]=0;
            max[4]=0;
            save=false;
            for (int j = 0; j < this.llf.size(); j++) {
                try {
                    if (this.llf.get(j).get(-starts[j]+i)=='A') max[0]+=1;
                    if (this.llf.get(j).get(-starts[j]+i)=='C') max[1]+=1;
                    if (this.llf.get(j).get(-starts[j]+i)=='T') max[2]+=1;
                    if (this.llf.get(j).get(-starts[j]+i)=='G') max[3]+=1;
                    if (this.llf.get(j).get(-starts[j]+i)=='-') max[4]+=1;
                    save=true;
                }catch(Exception e){}
            }
            if(save==false){
                end=false;
            }else{
                i++;
            }
            result+=getLetter(max);
        }
        //System.out.println("sequence of length : "+(result.length()-1));
        return result.substring(0, result.length()-1);
    }

    public void addAlignmentV2(Alignment input){
        
        String[] alignment={input.fragment1.toString(),input.fragment2.toString()};

        if(input.fragment1.equalStrip(new Fragment(this.llf.get(this.lm-1).toString()))){
            addLast(alignment);
        }
        else{
            //System.out.println("NEW");
            addNew(alignment);
        }
    }

    private void addNew(String[] alignment){
        
        LinkedList<Character>res1=new LinkedList<>();
        LinkedList<Character>res2=new LinkedList<>();
        
        for (int i = 0; i < alignment[0].length(); i++) {
            res1.addLast(alignment[0].charAt(i));
        }
        for (int i = 0; i < alignment[1].length(); i++){
            res2.addLast(alignment[1].charAt(i));
        }
        
        this.pos_prec=this.starts[this.lm-1]+this.llf.get(this.lm-1).size();
        addString(res1);
        addString(res2);
    }

    private void addLast(String[] alignment){

        LinkedList<Character> res1=new LinkedList<Character>();
        LinkedList<Character> res2=new LinkedList<Character>();

        for (int i = 0; i < alignment[0].length(); i++) {
            res1.addLast(alignment[0].charAt(i));
        }
        for (int i = 0; i < alignment[1].length(); i++){
            res2.addLast(alignment[1].charAt(i));
        }
        this.pos_prec=this.starts[this.lm-1]+nbrSpaces(this.llf.get(this.lm-1));
        // System.out.println("pos_prec : "+(this.starts[this.lm-1]+nbrSpaces(this.llf.get(this.lm-1))));
        gapGest(res1,res2);
    }

    private void gapGest(LinkedList<Character> res1,LinkedList<Character> res2){
        
        int index1=this.pos_prec;
        int index2=nbrSpaces(res1);
        while(true){
            try{
                if(!this.llf.get(this.lm-1).get(index1).equals(res1.get(index2))){
                    //System.out.println("======================================");
                    //System.out.println(this.llf.get(this.lm-1));
                    //System.out.println(res1);
                    //System.out.println("======================================");
                    if (this.llf.get(this.lm-1).get(index1).equals('-')) {
                        res1.add(index2,'-');
                        res2.add(index2,'-');
                    }else if(res1.get(index2).equals('-')){
                        for (int j = 0; j < this.llf.size(); j++) {
                            try{
                                this.llf.get(j).add(index1, '-');
                            }catch(Exception e){}
                        }
                    }
                }
                index1++;
                index2++;
            }catch(Exception e){
                break;
            }
        }
        this.pos_prec-=nbrSpaces(res1);
        addString(res2);
        updateStarts(this.lm-1);
    }

    private void updateStarts(int i){
        if(this.starts[i]<0){
            for (int j = 0; j < i+1; j++) {
                this.starts[j]+=-this.starts[i];
            }
        }
    }

    private void addString(LinkedList<Character> add){
        this.llf.addLast(add);
        this.starts[this.lm]=this.pos_prec;
        this.lm++;
    }

    private int nbrSpaces(LinkedList<Character> input){

        int result=0;

        for (int i = 0; i < input.size(); i++) {
            if(input.get(i).equals('-')){
                result++;
            }
            else{
                break;
            }
        }

        return result;
    }

    public String kappa(){
        String result="";
        for (int i = 0; i < this.llf.size(); i++) {
            for (int j = 0; j < this.starts[i]; j++) {
                result+="-";
            }
            for (int j = 0; j < this.llf.get(i).size(); j++) {
                result+=this.llf.get(i).get(j);
            }
            result+="\n";
        }
        return result;
    }

}