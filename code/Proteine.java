import java.util.Objects;

public class Proteine {

    byte prot;

    public Proteine(String x){

    }

    public Proteine(int prot){
        this.prot = (byte)prot;
    }

    public Proteine(byte prot){
        this.prot = (byte) prot;
    }

    public byte getByte(){
        return prot;
    }

    public void flip(){
        switch (prot){
            case 1:
                prot = 2;
                break;
            case 2:
                prot = 1;
                break;
            case 3:
                prot = 4;
                break;
            case 4:
                prot = 3;
                break;
            default:
                prot = 0;
        }
    }

    @Override
    public Proteine clone() {
        return new Proteine(this.prot);
    }

    @Override
    public String toString() {
        return String.valueOf(prot);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proteine proteine = (Proteine) o;
        return prot == proteine.prot;
    }

    @Override
    public int hashCode() {
        return Objects.hash(prot);
    }
}
