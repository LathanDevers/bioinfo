/**
 * ComputeWeightThread
 * @version 1
 * @author Daniels M.
 */
class ComputeAdjacenceMatrixThread extends Thread{
    
        private Adj adjacenceMatrix;
        private Fragment fragment1,fragment2;
        public final int GAPSCORE=-2;

        public ComputeAdjacenceMatrixThread(Fragment fragment1,Fragment fragment2,Adj adjacenceMatrix){
            
            this.fragment1=fragment1;
            this.fragment2=fragment2;
            this.adjacenceMatrix=adjacenceMatrix;
        }
    
        public void run(){
            for (int i = 1; i < this.adjacenceMatrix.getN(); i++) {
                for (int j = 1; j < this.adjacenceMatrix.getM(); j++) {
                    this.adjacenceMatrix.set(i,j,
                        Math.max(
                            this.adjacenceMatrix.get(i,j-1)+GAPSCORE,
                            Math.max(
                                this.adjacenceMatrix.get(i-1,j)+GAPSCORE,
                                this.adjacenceMatrix.get(i-1,j-1)+SemiV2.pScore(this.fragment1,this.fragment2,i-1,j-1/* character dans fragment1 i-1, character dans fragment2 j-1 */)
                            )
                        )
                    );
                }
            }
        }
    }