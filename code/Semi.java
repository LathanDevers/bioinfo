/*class Semi{

    private final int GAPSCORE = -2;

    String s, t;
    Adj tab;
    Res sm;
    int psf;

    public Semi(String s, String t){
        this.s = s;
        this.t=t;
        this.tab = new Adj(s, t);
        this.compute();
        System.out.println(tab.toString());
        this.sm=reconstruct();
    }

    public int gapScore(){
        return GAPSCORE;
    }

    public int pScore(Character si, Character tj){
        if (si.equals(tj))
            return 1;
        else
            return -1;
    }

    public void compute(){
        for (int i = 1; i<this.tab.getN(); i++){
            for (int j = 1; j<this.tab.getM(); j++){
                int temp1 = this.tab.get(i, j-1) + gapScore();
                int temp2 = this.tab.get(i-1, j) + gapScore();
                int temp3 = this.tab.get(i-1, j-1) + pScore(this.s.charAt(i-1), this.t.charAt(j-1));
                int res = Math.max(temp1, Math.max(temp2, temp3));
                tab.set(i, j, res);
            }
        }
    }

    public Res reconstruct(){
        Res res;


        String s1="";
        String s2="";

        Adj a=this.tab;

        int m1=0;
        int m2=0;
        int x1=0,x2=0,x=0;
        int y1=0,y2=0,y=0;
        int n=this.tab.getN()-1;
        int m=this.tab.getM()-1;
        int mx1=0,mx2=0,mx=0;
        int nx1=0,nx2=0,nx=0;
        
        for (int i = 0; i < m; i++) {
            if (m1<a.get(n,i)) {
                m1=a.get(n,i);
                y1=i-1;
                x1=n-1;
                mx1=i;
                nx1=n;
            }
        }
        for (int i = 0; i < n; i++) {
            if (m2<a.get(i,m)) {
                m2=a.get(i,m);
                y2=m-1;
                x2=i-1;
                mx2=m;
                nx2=i;
            }
        }

        if (m1>m2) {
            x=x1;
            y=y1;
            mx=mx1;
            nx=nx1;
            for (int i = m-1; i > y; i--) {
                s1="-"+s1;
                s2=this.t.charAt(i)+s2;
            }
        }
        else{
            x=x2;
            y=y2;
            mx=mx2;
            nx=nx2;
            for (int i = n-1; i > x; i--) {
                s2="-"+s2;
                s1=this.s.charAt(i)+s1;
            }
        }

        while (y>=0 && x>=0) {
            if (a.get(nx-1, mx)==a.get(nx,mx)-gapScore()) {
                s1=this.s.charAt(x)+s1;
                System.out.println(this.s.charAt(x));
                s2="-"+s2;
                x--;
                nx--;
            }
            else if (a.get(nx,mx-1)==a.get(nx,mx)-gapScore()) {
                s1="-"+s1;
                s2=this.t.charAt(y)+s2;
                System.out.println(this.s.charAt(x)+" "+this.t.charAt(y)+" "+a.get(nx,mx-1)+"="+a.get(nx,mx)+" "+gapScore());
                y--;
                mx--;
            }
            else if (a.get(nx-1,mx-1)==a.get(nx,mx)-pScore(this.s.charAt(x),this.t.charAt(y))){
                s1=this.s.charAt(x)+s1;
                s2=this.t.charAt(y)+s2;
                System.out.println(this.s.charAt(x)+" "+this.t.charAt(y)+" "+a.get(nx-1,mx-1)+"="+a.get(nx,mx)+" "+pScore(this.s.charAt(x),this.t.charAt(y)));
                x--;
                y--;
                nx--;
                mx--;
            }
        }

        while (x>=0) {
            s1=this.s.charAt(x)+s1;
            s2="-"+s2;
            x--;
        }

        while (y>=0) {
            s2=this.t.charAt(y)+s2;
            s1="-"+s1;
            y--;
        }

        res=new Res(s1,s2,m1,m2);

        System.out.println(res);

        return res;
    }
}

class Res{
    String s,t;
    int p1,p2;
    public Res(String s, String t, int p1,int p2){
        this.s=s;
        this.t=t;
        this.p1=p1;
        this.p2=p2;
    }

    public String toString(){
        String res="";
        res=res+this.s+"\n"+this.t+"\npoids s->t = "+this.p1+" / poids t->s = "+this.p2;
        return res;
    }
}*/