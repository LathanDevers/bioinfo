
class Adj{

    Fragment s;
    Fragment t;
    int n;
    int m;
    int[][] tab;

    /**
     * 
     * @param s string de taille n
     * @param t string de taille m
     */
    public Adj(Fragment s, Fragment t){
        this.s = s;
        this.n = s.getLength()+1;
        this.t = t;
        this.m = t.getLength()+1;
        this.tab = new int[n][m];
        this.init();
    }

    public Adj(int n,int m){
        this.tab = new int[n][m];
    }

    public void init(){
        for (int i = 0; i < this.n; i++){
            tab[i][0] = 0;
        }
        for (int j=0; j<this.m; j++){
            tab[0][j]=0;
        }
    }

    public int get(int i, int j){
        return tab[i][j];
    }

    public void set(int i, int j, int x){
        tab[i][j] = x;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public String toString(){
        String res = "";
        res += "\t\t";
        for (int i = 0; i < t.getLength();i++){
            res += " "+t.convertProt2St0(t.getChar(i))+ "\t";
        }
        res += "\n\t";
        for(int i=0;i<t.getLength()+1;i++){
            res+=" "+this.tab[0][i]+"\t";
        }
        res += "\n";
        for (int i = 1; i < getN(); i++){
            String temp = " "+s.convertProt2St0(s.getChar(i-1)) + "\t"+" "+this.tab[i][0]+"\t";
            for (int j = 1; j < getM(); j++){
                if (this.tab[i][j]>=0) {
                    temp += " "+this.tab[i][j] + "\t";
                }
                else{
                    temp += this.tab[i][j] + "\t";
                }
            }
            temp += "\n";
            res += temp;
        }
        return res;
    }
    
}